document.addEventListener('DOMContentLoaded', () => {
    const couponsContainer = document.querySelector('.coupon-container');
    const searchBox = document.querySelector('.search-box');
    const scrollToTopButton = document.createElement('button');
    scrollToTopButton.textContent = '↑';
    scrollToTopButton.className = 'scroll-to-top';
    document.body.appendChild(scrollToTopButton);

    const categoryElements = document.querySelectorAll('.category');
    let currentSearchQuery = '';  // Variable para almacenar el término de búsqueda actual

    // Load initial data
    loadInitialData();

    // Infinite scroll
    window.addEventListener('scroll', _.throttle(() => {
        handleScroll();
        if (window.scrollY > 300) {
            scrollToTopButton.classList.add('show');
        } else {
            scrollToTopButton.classList.remove('show');
        }
    }, 300));

    // Search functionality
    searchBox.addEventListener('input', _.debounce(() => {
        handleSearch();
    }, 300));

    // Scroll to top button
    scrollToTopButton.addEventListener('click', () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    });

    // Save scroll position on page unload
    window.addEventListener('beforeunload', () => {
        localStorage.setItem('scrollPosition', window.scrollY);
    });

    // Restore scroll position
    const lastScrollPosition = localStorage.getItem('scrollPosition');
    if (lastScrollPosition) {
        setTimeout(() => {
            window.scrollTo(0, parseInt(lastScrollPosition));
        }, 0);
    }

    // Add click event to categories
    categoryElements.forEach(category => {
        category.addEventListener('click', () => {
            const tag = category.getAttribute('data-tag');
            handleCategoryClick(tag);
        });
    });

    function loadInitialData() {
        let coupons = getCouponsFromLocalStorage();
        renderCoupons(coupons);
    }

    function handleScroll() {
        if (window.innerHeight + window.scrollY >= document.body.offsetHeight - 100) {
            loadMoreCoupons();
        }
    }

    function handleSearch() {
        currentSearchQuery = searchBox.value.toLowerCase();  // Actualiza el término de búsqueda actual
        let coupons = getCouponsFromLocalStorage();
        coupons = filterCouponsByQuery(coupons, currentSearchQuery);
        renderCoupons(coupons);
    }

    function handleCategoryClick(tag) {
        let coupons = getCouponsFromLocalStorage();
        coupons = coupons.filter(coupon => coupon.tags.some(couponTag => couponTag.toLowerCase() === tag.toLowerCase()));
        renderCoupons(coupons);
    }

    function loadMoreCoupons() {
        // Simulate loading more coupons (replace with actual data fetching)
        const moreCoupons = generateMoreCoupons();
        const coupons = getCouponsFromLocalStorage().concat(moreCoupons);
        saveCouponsToLocalStorage(coupons);

        // Filtrar los cupones recién cargados según el término de búsqueda actual
        const filteredCoupons = filterCouponsByQuery(coupons, currentSearchQuery);
        renderCoupons(filteredCoupons);
    }

    function filterCouponsByQuery(coupons, query) {
        return coupons.filter(coupon =>
            coupon.name.toLowerCase().includes(query) ||
            coupon.description.toLowerCase().includes(query) ||
            coupon.tags.some(tag => tag.toLowerCase().includes(query))
        );
    }

    function renderCoupons(coupons) {
        couponsContainer.innerHTML = '';
        // Sort coupons by creation date (newest first)
        coupons.sort((a, b) => b.timestamp - a.timestamp);
        let row;
        coupons.forEach((coupon, index) => {
            if (index % 5 === 0) {
                row = document.createElement('div');
                row.className = 'coupon-row';
                couponsContainer.appendChild(row);
            }
            const couponElement = document.createElement('div');
            couponElement.className = 'coupon';
            couponElement.innerHTML = `
                <div class="coupon-image"></div>
                <div class="coupon-details">
                    <h3>${coupon.name}</h3>
                    <p>${coupon.description}</p>
                    <p class="expiry">${coupon.expiry}</p>
                    <div class="coupon-footer">
                        <span class="price">${coupon.price}</span>
                        <button class="add-to-cart">Add to Cart</button>
                    </div>
                </div>
            `;
            row.appendChild(couponElement);
        });
    }

    function getCouponsFromLocalStorage() {
        return JSON.parse(localStorage.getItem('coupons') || '[]');
    }

    function saveCouponsToLocalStorage(coupons) {
        localStorage.setItem('coupons', JSON.stringify(coupons));
    }

    function generateMoreCoupons() {
        const timestamp = Date.now(); // Current timestamp
        return [
            {
                name: 'Find me',
                description: 'New description 1',
                expiry: 'Expires in 5 days',
                price: '$250',
                tags: ['toast', 'test'],
                timestamp
            },
            {
                name: 'New Coupon 2',
                description: 'New description 2',
                expiry: 'Expires in 5 days',
                price: '$250',
                tags: ['new', 'toast'],
                timestamp
            },
            {
                name: 'New Coupon 3',
                description: 'New description 3',
                expiry: 'Expires in 5 days',
                price: '$250',
                tags: ['future', 'sale'],
                timestamp
            },
            {
                name: 'New Coupon 4',
                description: 'New description 4',
                expiry: 'Expires in 5 days',
                price: '$250',
                tags: ['new', 'present'],
                timestamp
            },
            {
                name: 'New Coupon 5',
                description: 'New description 5',
                expiry: 'Expires in 5 days',
                price: '$250',
                tags: ['past', 'sale'],
                timestamp
            }
        ];
    }
});
